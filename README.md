# README #

### Eventshuffle API ###

* API for Eventshuffle service for scheduling events
* API is running at https://eventshuffleapi.azurewebsites.net/

### Stack ###

* .Net Core 3.0 
* Entity Framework
* Microsoft SQL Server
* XUnit
* Moq

### Remarks ###

* I had to downgrade from .Net Core 3.1, since it was causing problems while deploying
* Tests are not remotely comprehensive, they are more like and example at this point
* API validates the requests, but doesn't care if the data is logically incorrect (for example duplicate names are allowed, which in turn, break the results). This is intended, since the proper way to (in my opinion) for fixing this would be to register the users and using the id's instead of names, but this feels like out of scope for this assignment.
* Adding votes to unexisting dates will also result in a 200 OK, even though nothing gets added. Since the front end has already requested the dates, there shouldn't be possibility to add false dates. 