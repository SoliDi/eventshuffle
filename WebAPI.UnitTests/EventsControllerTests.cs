using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WebApi.Controllers;
using WebAPI.Interfaces;
using WebAPI.Models;
using Xunit;

namespace WebAPI.UnitTests
{
    public class EventsControllerTests
    {
        [Fact]
        public async Task List_Empty()
        {
            var eventsList = new EventsResponse();
            var eventService = new Mock<IEventService>();
            eventService.Setup(es => es.GetEvents()).Returns(Task.FromResult(eventsList));
            var eventsController = new EventController(eventService.Object);
            
            var result = await eventsController.List();
            var objectResult = result as OkObjectResult;
            Assert.IsType<OkObjectResult>(result);

            Assert.Empty(objectResult.Value as List<Event>);
        }
        
        [Fact]
        public async Task List_Basic()
        {
            var eventsList = new EventsResponse
            {
                Events = new List<SimpleEvent>
                {
                    new SimpleEvent() 
                    {
                        Id = 3,
                        Name = "Huge test event"
                    }
                }
                };
            var eventService = new Mock<IEventService>();
            eventService.Setup(es => es.GetEvents()).Returns(Task.FromResult(eventsList));
            var eventsController = new EventController(eventService.Object);
            
            var result = await eventsController.List();
            var objectResult = result as OkObjectResult;
            Assert.Equal(eventsList, objectResult.Value);
        }

        [Fact]
        public async Task Post_HappyCase()
        {
            var eventRequest = new EventRequest
            {
                Name = "Awesome party",
                Dates = new List<DateTime>{DateTime.Today}
            };
            
            var eventService = new Mock<IEventService>();
            var eventsController = new EventController(eventService.Object);
            
            var result = await eventsController.Post(eventRequest);
            
            Assert.IsType<OkObjectResult>(result);
        }
        
        [Fact]
        public async Task PostVote_HappyCase()
        {
            const long id = 1;
            var voteRequest = new VoteRequest
            {
                Name = "Awesome party",
                Votes =  new List<DateTime>
                {
                    DateTime.Today
                }
            };
            
            var shuffleEvent = new VoteResponse
            {
                Name = "test",
                Votes = new List<Vote>
                {
                    new Vote
                    {
                        Date = DateTime.Now,
                        Id = 2,
                        People = new List<string>
                        {
                            "Jake"
                        }
                    }
                }
            };

            var eventService = new Mock<IEventService>();
            eventService.Setup(es => es.AddVote(It.IsAny<long>(), It.IsAny<VoteRequest>())).Returns(Task.FromResult(shuffleEvent));

            var eventsController = new EventController(eventService.Object);
            
            var result = await eventsController.AddVote(id, voteRequest);

            var objectResult = result as OkObjectResult;
            
            Assert.Equal(shuffleEvent, objectResult.Value);
        }
    }
}