using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public class VoteResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<DateTime> Dates { get; set; }
        public virtual ICollection<Vote> Votes { get; set; }
    }
}