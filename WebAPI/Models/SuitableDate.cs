using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public class SuitableDate
    { 
        public DateTime Date { get; set; } 
        public IEnumerable<string> People { get; set; }
    }
}