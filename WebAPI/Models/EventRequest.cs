﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class EventRequest
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        public IEnumerable<DateTime> Dates { get; set; }
    }
}