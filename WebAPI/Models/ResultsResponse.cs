﻿using System.Collections.Generic;

namespace WebAPI.Models
{
    public class ResultsResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<SuitableDate> SuitableDates { get; set; } }
}