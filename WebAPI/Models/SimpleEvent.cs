namespace WebAPI.Models
{
    public class SimpleEvent
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}