﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class VoteRequest
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        public IEnumerable<DateTime> Votes { get; set; }
    }
}