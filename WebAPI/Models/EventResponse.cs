using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public class EventResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<DateTime> Dates { get; set; }
        public IEnumerable<Vote> Votes { get; set; }
    }
}