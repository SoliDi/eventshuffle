using System.Collections.Generic;

namespace WebAPI.Models
{
    public class EventsResponse
    {
        public IEnumerable<SimpleEvent> Events { get; set; }
    }
}