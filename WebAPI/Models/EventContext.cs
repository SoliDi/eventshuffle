﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Models
{
    public class EventContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                "Server=(localdb)\\mssqllocaldb;Database=Eventshuffle;Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vote>()
                .Property(v => v.People)
                .HasConversion(
                    votes => string.Join(';', votes),
                    vote => vote.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList());

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Event> Events { get; set; }
        public DbSet<Vote> Votes { get; set; }
    }
}