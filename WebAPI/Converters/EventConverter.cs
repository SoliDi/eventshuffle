using System;
using System.Collections.Generic;
using System.Linq;
using WebAPI.Models;

namespace WebAPI.Converters
{
    public class EventConverter
    {
        public static VoteResponse ToVoteResponse(Event shuffleEvent)
        {
            var dates = GetDatesFromVotes(shuffleEvent.Votes);
            var response = new VoteResponse
            {
                Id = shuffleEvent.Id,
                Dates = dates,
                Name = shuffleEvent.Name,
                Votes = ValidVotes(shuffleEvent.Votes)
            };

            return response;
        }
        
        public static EventResponse ToEventResponse(Event shuffleEvent)
        {
            var dates = GetDatesFromVotes(shuffleEvent.Votes);

            return new EventResponse
            {
                Id = shuffleEvent.Id,
                Dates = dates,
                Name = shuffleEvent.Name,
                Votes = ValidVotes(shuffleEvent.Votes)
            };
        }

        private static List<Vote> ValidVotes(IEnumerable<Vote> votes)
        {
            return votes.Where(v => v.People != null).ToList();
        }

        private static IEnumerable<DateTime> GetDatesFromVotes(IEnumerable<Vote> votes)
        {
            return votes?.Select(v => v.Date);
        }
    }
}