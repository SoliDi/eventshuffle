﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Interfaces;
using WebAPI.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/{controller}")]
    [ApiVersion("1.0")]
    public class EventController : Controller
    {
        private readonly IEventService _eventService;

        public EventController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpGet("list")]
        [ProducesResponseType(typeof(List<EventsResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> List()
        {
            var events = await _eventService.GetEvents();
            return Ok(events);
        }

        [HttpPost]
        [ProducesResponseType(typeof(CreateEventResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(EventRequest eventRequest)
        {
            var response = await _eventService.CreateEvent(eventRequest);
            return Ok(response);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(SimpleEvent), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Get(long id)
        {
            var response = await _eventService.GetEvent(id);
            return Ok(response);
        }

        [HttpPost("{id}/vote")]
        [ProducesResponseType(typeof(VoteResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddVote(long id, VoteRequest request)
        {
            var response = await _eventService.AddVote(id, request);
            
            return Ok(response);
        }

        [HttpGet("{id}/results")]
        [ProducesResponseType(typeof(ResultsResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Results(long id)
        {
            var response = await _eventService.Results(id);
            return Ok(response);
        }
    }
}