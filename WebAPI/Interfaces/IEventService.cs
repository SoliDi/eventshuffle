﻿using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Interfaces
{
    public interface IEventService
    {
        public Task<EventsResponse> GetEvents();
        public Task<EventResponse> GetEvent(long id);
        public Task<CreateEventResponse> CreateEvent(EventRequest request);
        public Task<VoteResponse> AddVote(long id, VoteRequest request);
        public Task<ResultsResponse> Results(long id);
    }
}