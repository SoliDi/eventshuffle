﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebAPI.Converters;
using WebAPI.Interfaces;
using WebAPI.Models;

namespace WebAPI.Services
{
    public class EventService : IEventService
    {
        private readonly EventContext _eventContext;
        
        public EventService(EventContext eventContext)
        {
            _eventContext = eventContext;
        }
        public async Task<EventsResponse> GetEvents()
        {
            var events = await _eventContext.Events.ToListAsync();
            var eventResponses = events
                .Select(e => new SimpleEvent {Id = e.Id, Name = e.Name});

            var eventsResponse = new EventsResponse {Events = eventResponses};
            
            return eventsResponse;
        }

        public async Task<CreateEventResponse> CreateEvent(EventRequest request)
        {
            var shuffleEvent = new Event{
            Name = request.Name,
            Votes = request.Dates.Select(date => new Vote {Date = date.Date}).ToList()
            };
            
            _eventContext.Events.Add(shuffleEvent);
            await _eventContext.SaveChangesAsync();

            return new CreateEventResponse {Id = shuffleEvent.Id};
        }

        public async Task<EventResponse> GetEvent(long id)
        { 
            var shuffleEvent = await _eventContext.Events.Include(e => e.Votes).
                FirstOrDefaultAsync(e => e.Id == id);

            return EventConverter.ToEventResponse(shuffleEvent);
        }

        public async Task<VoteResponse> AddVote(long id, VoteRequest request)
        {
            var shuffleEvent = await _eventContext.Events.Include(e => e.Votes)
                .FirstOrDefaultAsync(e => e.Id == id);
            
            var votes = request.Votes.Select(voteDate => shuffleEvent.Votes
                .FirstOrDefault(v => v.Date == voteDate));
            
            foreach (var vote in votes)
            {
                if (vote == null)
                {
                    continue;
                }
                
                if (vote.People == null)
                {
                    vote.People = new List<string>();
                }

                vote.People.Add(request.Name);
            }

            _eventContext.Votes.UpdateRange(shuffleEvent.Votes);
            await _eventContext.SaveChangesAsync();
    
            return EventConverter.ToVoteResponse(shuffleEvent);
        }

        public async Task<ResultsResponse> Results(long id)
        {
            var shuffleEvent = await _eventContext.Events.Include(e => e.Votes)
                .FirstOrDefaultAsync(e => e.Id == id);

            if (shuffleEvent == null)
            {
                return null;
            }

            var distinctPeople = shuffleEvent.Votes.Where(v => v.People != null)
                .SelectMany(v => v.People).Distinct().ToList();

            if (!distinctPeople.Any())
            {
                return null;
            }

            var suitableVotes = shuffleEvent.Votes.Where(v => v.People != null &&
                                                              v.People.Count() == distinctPeople.Count).ToList();
            var dates = suitableVotes.Select(v => v.Date);

            var people = suitableVotes.SelectMany(sv => sv.People).Distinct();

            var suitableDates = dates.Select(date => new SuitableDate {Date = date, People = people});

            var response = new ResultsResponse
            {
                Id = shuffleEvent.Id,
                Name = shuffleEvent.Name,
                SuitableDates = suitableDates
            };

            return response;
        }
    }
}